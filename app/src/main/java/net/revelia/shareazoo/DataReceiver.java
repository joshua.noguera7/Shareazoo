package net.revelia.shareazoo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class DataReceiver extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_data_receiver);

        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            }
        }
    }
    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);

        if (sharedText != null) {
            // TextView dataText = findViewById(R.id.textView);
            // dataText.setText(sharedText); //set text for text view
            Toast.makeText(this,
                    "Queuing: " + sharedText, Toast.LENGTH_SHORT).show();
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(DataReceiver.this);
            String url = "https://musicazoo.mit.edu/enqueue?youtube_id=" + sharedText;
            url = "https://musicazoo.mit.edu/enqueue";
            // url ="https://www.google.com";
            // dataText.setText(url);
            // Request a string response from the provided URL.

            StringRequest MyStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //This code is executed if the server responds, whether or not the response contains data.
                    //The String 'response' contains the server's response.
                    // dataText.setText(response);
                }
            }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
                @Override
                public void onErrorResponse(VolleyError error) {
                    //This code is executed if there is an error.
                    // dataText.setText("error: " + error);
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> MyData = new HashMap<String, String>();
                    MyData.put("youtube_id", sharedText); //Add the data you'd like to send to the server.
                    return MyData;
                }
            };

// Add the request to the RequestQueue.
            queue.add(MyStringRequest);
            finish();



        }
    }

}